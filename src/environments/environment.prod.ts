export const environment = {
  production: true,
  backUrl: 'https://backend.centauria.com.mx/api/',
  frontUrl: 'https://we.centauria.com.mx/',
};

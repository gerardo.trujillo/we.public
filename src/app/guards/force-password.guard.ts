import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from "../interfaces/user";
import { StorageService } from "../services/storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class ForcePasswordGuard implements CanActivate {
  user:User=<User>{};

  constructor(private router: Router,
              private storage: StorageService) {
    this.user = this.storage.getUser();
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this.user) {
      this.router.navigateByUrl('/errors/E403');
      return false;
    }
    return true;
  }

}

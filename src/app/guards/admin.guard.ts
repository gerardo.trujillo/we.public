import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from "../services/storage/storage.service";
import {User} from "../interfaces/user";

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  user:User=<User>{};
  access:boolean=false;

  constructor(private router: Router,
              private storage: StorageService) {
    this.user = this.storage.getUser();
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.user.role == 'ADMIN_ROLE' || this.user.role == 'SUPER_ADMIN_ROLE'){
      this.access = true;
    }
    if (!this.access) {
      this.router.navigateByUrl('/errors/E403');
      return false;
    }
    return true;
  }

}

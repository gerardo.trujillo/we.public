import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  baseUrl = environment.baseUrl;
  headers: any;
  token: string = '';

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
  }

  getUser(token: string){
    this.headers = new HttpHeaders({
      'x-token': token,
    });
    return this.http.get<any>(this.baseUrl + '/users/user', {headers: this.headers});
  }
}

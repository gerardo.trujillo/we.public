import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class PasswordService {

  url = environment.backUrl + 'password';
  token = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
  }

  findUser(params: any){
    return this.http.post<any>(`${this.url}/find/user`, params);
  }

  getUser(id:string){
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.get<any>(`${this.url}/${id}`, {headers: this.headers});
  }

  changePassword(params: any){
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.post<any>(`${this.url}`, params, {headers: this.headers});
  }

  changePasswordToken(params: any){
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.post<any>(`${this.url}/token`, params, {headers: this.headers});
  }

  tokenFind(params: any){
    return this.http.post<any>(`${this.url}/find/token`, params);
  }

  resetPassword(params: any){
    return this.http.post<any>(this.url + '/reset', params);
  }
}

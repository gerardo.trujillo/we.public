import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {StorageService} from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class AreasService {

  url = environment.backUrl + 'areas';
  token = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
  }


  getAreas(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }

  showArea(id: string){
    return this.http.get<any>(`${this.url}/${id}`, {headers: this.headers});
  }

  activeArea(params: any){
    return this.http.post<any>(`${this.url}/active`,params, {headers: this.headers});
  }

  postArea(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putArea(id: string, params: any){
    return this.http.put<any>(`${this.url}/${id}`, params, {headers: this.headers});
  }

  deleteArea(id: string){
    return this.http.delete<any>(`${this.url}/${id}`, {headers: this.headers});
  }
}

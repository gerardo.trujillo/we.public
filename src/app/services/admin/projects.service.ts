import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {StorageService} from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  url = environment.backUrl + 'projects';
  token = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
  }


  getProjects(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }

  showProject(id: string){
    return this.http.get<any>(`${this.url}/${id}`, {headers: this.headers});
  }

  activeProject(params: any){
    return this.http.post<any>(`${this.url}/active`,params, {headers: this.headers});
  }

  postProject(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putProject(id: string, params: any){
    return this.http.put<any>(`${this.url}/${id}`, params, {headers: this.headers});
  }

  deleteProject(id: string){
    return this.http.delete<any>(`${this.url}/${id}`, {headers: this.headers});
  }
}

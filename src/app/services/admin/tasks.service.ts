import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { StorageService } from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  url = environment.backUrl + 'tasks';
  token = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
  }

  getTasks(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }

  getTask(id: string){
    return this.http.get<any>(`${this.url}/${id}`, {headers: this.headers});
  }

  activeTask(params: any){
    return this.http.post<any>(`${this.url}/active`, params,{headers: this.headers});
  }

  postTask(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putTask(id: any, params: any){
    return this.http.put<any>(`${this.url}/${id}`, params, {headers: this.headers});
  }

  deleteTask(id: string){
    return this.http.delete<any>(`${this.url}/${id}`, {headers: this.headers});
  }
}

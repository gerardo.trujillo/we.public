import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  baseUrl = environment.backUrl + 'categories/public';

  constructor(private http: HttpClient) {

  }

  getData(){
    return this.http.get<any>(this.baseUrl);
  }

  getCategory(slug: string){
    return this.http.get<any>(`${this.baseUrl}/${slug}`);
  }

}

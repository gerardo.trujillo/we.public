import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  url = environment.backUrl + 'news/public';
  token = '';
  headers:any;

  constructor(private http: HttpClient) {
  }

  getNews(){
    return this.http.get<any>(this.url);
  }


  getAllNews(id: string){
    return this.http.get<any>(this.url + '/all');
  }

  showNews(slug: string){
    return this.http.get<any>(`${this.url}/${slug}`);
  }


}

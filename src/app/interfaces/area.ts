import { Project } from "./project";

export interface Area {
  _id: string;
  name: {
    es: string,
    en:string,
  },
  slug: string;
  date: Date
  description: {
    es: string,
    en:string,
  },
  project: Project
  image: string;
  images: any[];
  active: boolean;
  delete: boolean;
}

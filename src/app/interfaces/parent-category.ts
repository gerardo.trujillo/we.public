export interface ParentCategory {
  _id:string;
  name: {
    es: string,
    en: string,
  }
}

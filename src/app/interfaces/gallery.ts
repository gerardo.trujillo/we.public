export interface Gallery {
  id: number;
  name: {
    es: string;
    en: string;
  };
  slug: {
    es: string;
    en: string;
  };
  type: string;
  reference: string;
  active: boolean
}

export interface Kind {
  _id: string;
  name: string;
  slug: string;
  image: string;
  parent_id: string;
  active: boolean;
  delete: boolean;
  status: boolean;
}

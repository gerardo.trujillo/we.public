export interface Project {
  _id: string;
  name: {
    es: string,
    en:string,
  },
  slug: string;
  date: Date
  description: {
    es: string,
    en:string,
  },
  image: string;
  images: any[];
  active: boolean;
  delete: boolean
}

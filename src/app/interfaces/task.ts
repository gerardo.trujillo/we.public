import { Category } from "./category";
import { User } from "./user";
import { Area } from "./area";

export interface Task {
  _id: string;
  name: {
    es: string;
    en: string;
  };
  slug: string;
  date: Date;
  description: {
    es: string;
    en: string;
  };
  complexity: {
    es: string;
    en: string;
  };
  include: {
    es: string;
    en: string;
  };
  changes: {
    es: string;
    en: string;
  };
  editable: boolean;
  level: {
    es: string;
    en: string;
  };
  price: number;
  version: {
    es: string;
    en: string;
  };
  proposal: {
    es: string;
    en: string;
  };
  size: number;
  area: Area;
  user: User;
  delete: boolean;
  image: string;
  record: string;
  active: boolean;
  categories: Category[];
  images: any[];
}

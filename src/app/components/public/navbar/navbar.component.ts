import {Component, HostListener, OnInit} from '@angular/core';
import { faBars, faTimes } from "@fortawesome/free-solid-svg-icons";
import { CategoriesService } from "../../../services/public/categories.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ToastService} from "../../../services/components/toast.service";
import {Category} from "../../../interfaces/category";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  @HostListener("window:scroll", ['$event'])
  onWindowScroll(event: Event) {
    if (window.scrollY > 70){
      this.toggleBg(true);
    } else {
      this.toggleBg(false);
    }
  }

  faMenu = faBars;
  open = false;
  bgOption: boolean = false;
  categories:Category[]=[];

  constructor(private service: CategoriesService,
              private loading: NgxSpinnerService,
              private toastService: ToastService) { }

  ngOnInit(): void {
    this.getData();
  }

  toggleMenu(){
    if (this.open){
      this.open = false;
      this.faMenu = faBars;
    } else {
      this.open = true;
      this.faMenu = faTimes;
    }
  }

  getData(){
    this.loading.show();
    this.service.getData().subscribe(response => {
      this.categories = response.categories;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

  toggleBg(option: boolean){
    this.bgOption = option;
  }

}

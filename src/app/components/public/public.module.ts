import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from "@angular/router";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { BreadcrumbPublicComponent } from "./breadcrumb/breadcrumb.component";



@NgModule({
  declarations: [
    NavbarComponent,
    FooterComponent,
    BreadcrumbPublicComponent
  ],
  exports: [
    NavbarComponent,
    FooterComponent,
    BreadcrumbPublicComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule
  ]
})
export class PublicComponentsModule { }

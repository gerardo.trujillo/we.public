import { Component, OnInit } from '@angular/core';
import { faTable, faPlus, faPowerOff, faTrashAlt, faCertificate, faStar } from '@fortawesome/free-solid-svg-icons';
import { Title } from "@angular/platform-browser";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastService } from "../../../../services/components/toast.service";
import { ProjectsService } from "../../../../services/admin/projects.service";
import { TranslationService } from "../../../../services/admin/translation.service";
import { Project } from "../../../../interfaces/project";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faCertificate = faCertificate;
  faStar = faStar;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  projects: Project[]=[];
  public page: number | undefined;

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private toastService: ToastService,
              private service: ProjectsService,
              public serviceTranslation: TranslationService) {
    this.titleService.setTitle("Lista de Proyectos");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.service.getProjects().subscribe(response => {
      this.projects = response.projects;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  active(project: string, option: boolean){
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('id', project);
    params.append('option', option.toString());
    this.service.activeProject(params).subscribe(response => {
      if (response.project.active){
        if (language == 'es'){
          this.showSuccess(`El proyecto ${response.project.name.es} se activo con exito`);
        } else if (language == 'en'){
          this.showSuccess(`El proyecto ${response.project.name.en} se activo con exito`);
        }
      } else {
        if (language == 'es'){
          this.showSuccess(`El proyecto ${response.project.name.es} se des-activo con exito`);
        } else if (language == 'en'){
          this.showSuccess(`El proyecto ${response.project.name.en} se des-activo con exito`);
        }
      }
      this.getData();
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  delete(id: string){
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    this.service.deleteProject(id).subscribe(response => {
      if (language == 'es'){
        this.showSuccess(`El Proyecto ${response.project.name.es} se elimino con exito`);
      } else if (language == 'en'){
        this.showSuccess(`El Proyecto ${response.project.name.en} se elimino con exito`);
      }
      this.getData();
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light' });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light' });
  }

}

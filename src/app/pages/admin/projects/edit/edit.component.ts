import { Component, OnInit } from '@angular/core';
import { faList, faSave, faEye, faEyeSlash, faImages, faHandPointer, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { NgForm } from '@angular/forms';
import { TranslationService } from '../../../../services/admin/translation.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {ActivatedRoute, Router} from '@angular/router';
import { environment } from "../../../../../environments/environment";
import { Title } from "@angular/platform-browser";
import { StorageService } from "../../../../services/storage/storage.service";
import { ToastService } from "../../../../services/components/toast.service";
import { ProjectsService } from "../../../../services/admin/projects.service";
import { Project } from "../../../../interfaces/project";
import { UploadsService } from "../../../../services/admin/uploads.service";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  faList = faList;
  faTrashAlt = faTrashAlt;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  url_images = environment.backUrl;
  public Editor = ClassicEditor;
  project:Project=<Project>{
    name: {
      es:'',
      en:''
    },
    description:{
      es:'',
      en:''
    }
  };
  image:any;
  imageInit:any;
  thumbnail:any;
  id:any;
  name:any;
  description:any;
  editImage = true;
  files: File[] = [];
  images: any[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: ProjectsService,
              private uploads: UploadsService,
              private loading: NgxSpinnerService,
              private router: Router,
              private toastService: ToastService,
              private titleService: Title,
              private storage: StorageService,
              private activatedRouter: ActivatedRoute) {
    this.titleService.setTitle("Crear Projecto");
    this.activatedRouter.params.subscribe(params => {
      this.getData(params['project']);
    });
  }

  ngOnInit(): void {
  }

  getData(project:string){
    this.loading.show();
    this.service.showProject(project).subscribe(response => {
      console.log(response);
      this.project = response.project;
      this.imageInit = response.project.image;
      this.images = response.project.images;
      this.loading.hide();
    }, error => {
      if (error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    const params= new FormData();
    params.append('Content-Type', 'multipart/form-data');
    // @ts-ignore
    params.append('file', ...event.addedFiles);
    this.uploads.postUploadGallery(this.project._id, 'projects', params).subscribe(response => {
      this.showSuccess('La imagen se agrego con exito');
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('description', form.value.description);
    if (this.image){
      params.append('file', this.image);
    }
    params.append('language', language);
    this.service.putProject(this.project._id, params).subscribe(response => {
      if (language == 'es'){
        this.showSuccess(`El proyecto ${response.project.name.es} se actualizo con exito`);
      } else if (language == 'en'){
        this.showSuccess(`El proyecto ${response.project.name.es} se actualizo con exito`);
      }
      this.router.navigateByUrl('/admin/projects');
      this.loading.hide();
    }, error => {
      if (error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  deleteImageGallery(img: string){
    this.loading.show();
    this.uploads.deleteUploadGallery('projects', this.project._id, img).subscribe(response => {
      this.getData(this.project._id);
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  deleteImage(img: string){
    this.loading.show();
    this.uploads.deleteUpload('projects', this.project._id, img).subscribe(response => {
      this.getData(this.project._id);
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  deleteImageThumbnail(){
    this.thumbnail = undefined;
    this.image = undefined;
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light'});
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light' });
  }

}

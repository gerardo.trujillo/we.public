import { Component, OnInit } from '@angular/core';
import { faList, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { NgForm } from '@angular/forms';
import { TranslationService } from '../../../../services/admin/translation.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { environment } from "../../../../../environments/environment";
import { Title } from "@angular/platform-browser";
import { StorageService } from "../../../../services/storage/storage.service";
import { ToastService } from "../../../../services/components/toast.service";
import { AreasService } from "../../../../services/admin/areas.service";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {ProjectsService} from "../../../../services/admin/projects.service";
import {Project} from "../../../../interfaces/project";


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  url_images = environment.backUrl;
  public Editor = ClassicEditor;
  image:any;
  imageInit:any;
  thumbnail:any;
  projects:Project[]=[];
  id:any;
  name:any;
  description:any;
  editImage = true;
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: AreasService,
              private serviceProject: ProjectsService,
              private loading: NgxSpinnerService,
              private router: Router,
              private toastService: ToastService,
              private titleService: Title,
              private storage: StorageService) {
    this.titleService.setTitle("Crear Area");
    this.getData();
  }

  ngOnInit(): void {
  }

  getData(){
    this.loading.show();
    this.serviceProject.getProjects().subscribe(response => {
      this.projects = response.projects;
      this.loading.hide();
    }, error => {
      if (error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    // this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('description', form.value.description);
    params.append('projectId', form.value.project);
    if (this.image){
      params.append('file', this.image);
    }
    params.append('language', language);
    this.service.postArea(params).subscribe(response => {
      console.log(response);
      if (language == 'es'){
        this.showSuccess(`La area ${response.area.name.es} se creo con exito`);
      } else if (language == 'en'){
        this.showSuccess(`La area ${response.area.name.es} se creo con exito`);
      }
      this.router.navigateByUrl('/admin/areas');
      this.loading.hide();
    }, error => {
      if (error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light'});
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light' });
  }

}

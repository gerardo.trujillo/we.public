import { Component, OnInit } from '@angular/core';
import { faList, faSave, faEye, faEyeSlash, faImages, faHandPointer, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { environment } from "../../../../../environments/environment";
import { Project } from "../../../../interfaces/project";
import { TranslationService } from "../../../../services/admin/translation.service";
import { AreasService } from "../../../../services/admin/areas.service";
import { ProjectsService } from "../../../../services/admin/projects.service";
import { NgxSpinnerService } from "ngx-spinner";
import {ActivatedRoute, Router} from "@angular/router";
import { ToastService } from "../../../../services/components/toast.service";
import { Title } from "@angular/platform-browser";
import { StorageService } from "../../../../services/storage/storage.service";
import { NgForm } from "@angular/forms";
import { Area } from "../../../../interfaces/area";
import { UploadsService } from "../../../../services/admin/uploads.service";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  faList = faList;
  faTrashAlt = faTrashAlt;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  url_images = environment.backUrl;
  public Editor = ClassicEditor;
  image:any;
  imageInit:any;
  thumbnail:any;
  projects:Project[]=[];
  id:any;
  area:Area=<Area>{
    name: {
      es: '',
      en:''
    },
    description: {
      es: '',
      en: ''
    },
    project: {
      _id: ''
    },
  };
  name:any;
  description:any;
  editImage = true;
  files: File[] = [];
  images: any[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: AreasService,
              private uploads: UploadsService,
              private serviceProject: ProjectsService,
              private loading: NgxSpinnerService,
              private router: Router,
              private toastService: ToastService,
              private titleService: Title,
              private storage: StorageService,
              private activatedRoute: ActivatedRoute) {
    this.titleService.setTitle("Crear Area");
    this.getProjects();
    this.activatedRoute.params.subscribe(params => {
      this.getData(params['area']);
    });
  }

  ngOnInit(): void {
  }

  getData(area:string){
    this.loading.show();
    this.service.showArea(area).subscribe(response => {
      this.area = response.area;
      this.imageInit = response.area.image;
      this.images = response.area.images;
      this.loading.hide();
    }, error => {
      if (error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  getProjects(){
    this.loading.show();
    this.serviceProject.getProjects().subscribe(response => {
      this.projects = response.projects;
      this.loading.hide();
    }, error => {
      if (error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    const params= new FormData();
    params.append('Content-Type', 'multipart/form-data');
    // @ts-ignore
    params.append('file', ...event.addedFiles);
    this.uploads.postUploadGallery(this.area._id, 'areas', params).subscribe(response => {
      this.showSuccess('La imagen se agrego con exito');
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('description', form.value.description);
    params.append('projectId', form.value.project);
    if (this.image){
      params.append('file', this.image);
    }
    params.append('language', language);
    this.service.putArea(this.area._id, params).subscribe(response => {
      if (language == 'es'){
        this.showSuccess(`La area ${response.area.name.es} se creo con exito`);
      } else if (language == 'en'){
        this.showSuccess(`La area ${response.area.name.es} se creo con exito`);
      }
      this.router.navigateByUrl('/admin/areas');
      this.loading.hide();
    }, error => {
      if (error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  deleteImageGallery(img: string){
    this.loading.show();
    this.uploads.deleteUploadGallery('areas', this.area._id, img).subscribe(response => {
      this.getData(this.area._id);
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  deleteImage(img: string){
    this.loading.show();
    this.uploads.deleteUpload('areas', this.area._id, img).subscribe(response => {
      this.getData(this.area._id);
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  deleteImageThumbnail(){
    this.thumbnail = undefined;
    this.image = undefined;
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light'});
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light' });
  }

}

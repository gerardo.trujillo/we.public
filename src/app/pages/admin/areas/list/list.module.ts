import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './list.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgxPaginationModule } from "ngx-pagination";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AdminComponentsModule } from "../../../../components/admin/admin-components.module";
import { NgxDropzoneModule } from "ngx-dropzone";
import { FormsModule } from "@angular/forms";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { PipesModule } from "../../../../pipes/pipes.module";


@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    CommonModule,
    ListRoutingModule,
    FontAwesomeModule,
    NgxPaginationModule,
    NgbModule,
    AdminComponentsModule,
    NgxDropzoneModule,
    FormsModule,
    CKEditorModule,
    PipesModule
  ]
})
export class ListModule { }

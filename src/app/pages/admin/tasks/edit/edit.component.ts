import { Component, OnInit } from '@angular/core';
import { faList, faPowerOff, faTrashAlt, faSave, faEye, faEyeSlash, faImages, faEdit } from '@fortawesome/free-solid-svg-icons';
import { TranslationService } from "../../../../services/admin/translation.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute, Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { environment } from "../../../../../environments/environment";
import { Title} from "@angular/platform-browser";
import { TasksService } from "../../../../services/admin/tasks.service";
import { Area } from "../../../../interfaces/area";
import { Task } from "../../../../interfaces/task";
import { GalleriesService } from "../../../../services/admin/galleries.service";
import { CategoriesService } from "../../../../services/admin/categories.service";
import { ToastService } from "../../../../services/components/toast.service";
import { UploadsService } from "../../../../services/admin/uploads.service";
import { AreasService } from "../../../../services/admin/areas.service";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  faList = faList;
  faEdit = faEdit;
  faImages = faImages;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  url_images = environment.backUrl;
  public Editor = ClassicEditor;
  images: any[] = [];
  image:any;
  imageInit:any;
  thumbnail:any;
  task:Task = <Task>{
    name: {
      es: '',
      en: '',
    },
    description: {
      es: '',
      en: '',
    },
    complexity: {
      es: '',
      en: '',
    },
    include: {
      es: '',
      en: '',
    },
    changes: {
      es: '',
      en: '',
    },
    level: {
      es: '',
      en: '',
    },
    version: {
      es: '',
      en: '',
    },
    proposal: {
      es: '',
      en: '',
    },
    area: {
      _id:''
    }
  };
  areas: Area[]= [];
  editable = true;
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: TasksService,
              private loading: NgxSpinnerService,
              private areasService: AreasService,
              private toastService: ToastService,
              public uploads: UploadsService,
              private categoriesService: CategoriesService,
              private router: Router,
              private titleService: Title,
              private serviceImage: GalleriesService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['id']);
      this.getAreas();
    });
  }

  ngOnInit(): void {
  }

  getAreas(){
    this.loading.show();
    this.areasService.getAreas().subscribe(response => {
      this.areas = response.areas;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getData(id: string){
    this.loading.show();
    this.service.getTask(id).subscribe(response => {
      this.task = response.task;
      this.imageInit = response.task.image;
      this.images = response.task.images;
      this.editable = response.task.editable;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    const params= new FormData();
    params.append('Content-Type', 'multipart/form-data');
    // @ts-ignore
    params.append('file', ...event.addedFiles);
    this.uploads.postUploadGallery(this.task._id, 'tasks', params).subscribe(response => {
      this.showSuccess('La imagen se agrego con exito');
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  onRemove(event: any) {
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('description', form.value.description);
    params.append('complexity', form.value.complexity);
    params.append('include', form.value.include);
    params.append('changes', form.value.changes);
    params.append('editable', this.editable.toString());
    params.append('level', form.value.level);
    params.append('price', form.value.price);
    params.append('version', form.value.version);
    params.append('proposal', form.value.proposal);
    params.append('size', form.value.size);
    params.append('areaId', form.value.area);
    if (this.image){
      params.append('file', this.image);
    }
    params.append('language', language);
    this.service.putTask(this.task._id, params).subscribe(response => {
      if (language == 'es'){
        this.showSuccess(`LaTarea ${response.task.name.es} se actualizo con exito`);
      } else if(language == 'en'){
        this.showSuccess(`LaTarea ${response.task.name.en} se actualizo con exito`);
      }
      this.router.navigateByUrl('/admin/tasks');
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }


  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  deleteImageGallery(img: string){
    this.loading.show();
    this.uploads.deleteUploadGallery('tasks', this.task._id, img).subscribe(response => {
      this.getData(this.task._id);
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  deleteImage(img: string){
    this.loading.show();
    this.uploads.deleteUpload('tasks', this.task._id, img).subscribe(response => {
      this.getData(this.task._id);
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  deleteImageThumbnail(){
    this.thumbnail = undefined;
    this.image = undefined;
  }

  setEditable(){
    if(this.editable){
      this.editable = false;
    } else {
      this.editable = true;
    }
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}

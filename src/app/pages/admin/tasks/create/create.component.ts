import { Component, OnInit } from '@angular/core';
import { faList, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { TranslationService } from "../../../../services/admin/translation.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { Title} from "@angular/platform-browser";
import { TasksService } from "../../../../services/admin/tasks.service";
import { Area } from "../../../../interfaces/area";
import { ToastService } from "../../../../services/components/toast.service";
import { AreasService } from "../../../../services/admin/areas.service";
import { StorageService } from "../../../../services/storage/storage.service";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';






@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  public Editor = ClassicEditor;
  image:any;
  imageInit:any;
  thumbnail:any;
  areas: Area[]= [];
  editable = false;
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private areasService: AreasService,
              private service: TasksService,
              private loading: NgxSpinnerService,
              private toastService: ToastService,
              private router: Router,
              private titleService: Title) {
    this.titleService.setTitle("Crear Tarea");
  }

  ngOnInit(): void {
    this.getAreas();
  }

  getAreas(){
    this.loading.show();
    this.areasService.getAreas().subscribe(response => {
      this.areas = response.areas;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('description', form.value.description);
    params.append('complexity', form.value.complexity);
    params.append('include', form.value.include);
    params.append('changes', form.value.changes);
    params.append('editable', this.editable.toString());
    params.append('level', form.value.level);
    params.append('price', form.value.price);
    params.append('version', form.value.version);
    params.append('proposal', form.value.proposal);
    params.append('size', form.value.size);
    params.append('areaId', form.value.area);
    if (this.image){
      params.append('file', this.image);
    }
    params.append('language', language);
    this.service.postTask(params).subscribe(response => {
      console.log(response);
      if (language == 'es'){
        this.showSuccess(`La Tarea ${response.task.name.es} se creo con exito`);
      } else if(language == 'en'){
        this.showSuccess(`La Tarea ${response.task.name.en} se creo con exito`);
      }
      this.router.navigateByUrl('/admin/tasks');
      this.loading.hide();
    }, error => {
      if (error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  setEditable(){
    if(this.editable){
      this.editable = false;
    } else {
      this.editable = true;
    }
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TasksRoutingModule } from './tasks-routing.module';
import { TasksComponent } from './tasks.component';
import { ListComponent } from "./list/list.component";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgxPaginationModule } from "ngx-pagination";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgxDropzoneModule } from "ngx-dropzone";
import { FormsModule } from "@angular/forms";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { CategoriesComponent } from './categories/categories.component';
import { AdminComponentsModule } from "../../../components/admin/admin-components.module";
import { PipesModule } from "../../../pipes/pipes.module";


@NgModule({
  declarations: [
    TasksComponent,
    ListComponent,
    CreateComponent,
    EditComponent,
    CategoriesComponent
  ],
    imports: [
        CommonModule,
        TasksRoutingModule,
        FontAwesomeModule,
        NgxPaginationModule,
        NgbModule,
        AdminComponentsModule,
        NgxDropzoneModule,
        FormsModule,
        CKEditorModule,
        PipesModule
    ]
})
export class TasksModule { }

import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { NgxSpinnerService } from "ngx-spinner";
import { faTable, faPlus, faPowerOff, faTrashAlt, faCertificate, faStar } from '@fortawesome/free-solid-svg-icons';
import { TasksService } from "../../../../services/admin/tasks.service";
import { Task } from "../../../../interfaces/task";
import { environment } from "../../../../../environments/environment";
import { TranslationService } from "../../../../services/admin/translation.service";
import {ToastService} from "../../../../services/components/toast.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faCertificate = faCertificate;
  faStar = faStar;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  tasks: Task[]=[];
  public page: number | undefined;
  url_images = environment.backUrl;

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private toastService: ToastService,
              private service: TasksService,
              public serviceTranslation: TranslationService) {
    this.titleService.setTitle("Lista de Tareas");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
   this.loading.show();
    this.service.getTasks().subscribe(response => {
      this.tasks = response.tasks;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  active(task: string, option: boolean){
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('id', task);
    params.append('option', option.toString());
      this.service.activeTask(params).subscribe(response => {
        if (response.task.active){
          if (language == 'es'){
            this.showSuccess(`La Tarea ${response.task.name.es} se activo con exito`);
          } else if (language == 'en'){
            this.showSuccess(`La Tarea ${response.task.name.en} se activo con exito`);
          }
        } else {
          if (language == 'es'){
            this.showSuccess(`La Tarea ${response.task.name.es} se des-activo con exito`);
          } else if (language == 'en'){
            this.showSuccess(`La Tarea ${response.task.name.en} se des-activo con exito`);
          }
        }
        this.getData();
        this.loading.hide();
      }, error => {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
        console.log(error);
        this.loading.hide();
      });
  }

  delete(id: string){
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    this.service.deleteTask(id).subscribe(response => {
      if (language == 'es'){
        this.showSuccess(`a Tarea ${response.task.name.es} se elimino con exito`);
      } else if (language == 'en'){
        this.showSuccess(`a Tarea ${response.task.name.en} se elimino con exito`);
      }
      this.getData();
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}

import {Component, forwardRef, Input, OnInit, ViewChild} from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslationService } from '../../../../services/admin/translation.service';
import { CategoriesService } from '../../../../services/admin/categories.service';
import { faPlus, faWarehouse } from '@fortawesome/free-solid-svg-icons';
import { faPowerOff, faTrashAlt,  } from '@fortawesome/free-solid-svg-icons';
import {environment} from '../../../../../environments/environment';
import { Alert } from "../../../../interfaces/alert";
import { StorageService } from "../../../../services/storage/storage.service";
import {ToastService} from "../../../../services/components/toast.service";

@Component({
  selector: 'app-subcateries-categories',
  templateUrl: './subcateries-categories.component.html',
  styleUrls: ['./subcateries-categories.component.sass']
})
export class SubcateriesCategoriesComponent implements OnInit {

  @Input() parent!: string;
  // @ts-ignore
  @ViewChild(SubcateriesCategoriesComponent) SubcateriesCategoriesComponent: SubcateriesCategoriesComponent;
  faPlus = faPlus;
  faWarehouse = faWarehouse;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  url_images = environment.backUrl;
  alert:Alert =<Alert>{};
  categories:any[]=[];
  categoriesAll:any[]=[];

  constructor(private service : CategoriesService,
              private loading: NgxSpinnerService,
              public serviceTranslation: TranslationService,
              private toastService: ToastService,
              private storage: StorageService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.service.getCategoriesChildren(this.parent).subscribe(response => {
      this.categories = response.categories;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });

  }

  active(category: string, value: boolean){
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    if (value){
      this.service.desActiveCategory(category).subscribe(response => {
        if (language == 'es'){
          this.showSuccess(`La categoria ${response.category.name.es} se desactivo con exito`);
        } else if (language == 'en'){
          this.showSuccess(`La categoria ${response.category.name.en} se desactivo con exito`);
        }
        this.getData();
        this.loading.hide();
      }, error => {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
        console.log(error);
        this.loading.hide();
      });
    } else {
      this.service.activeCategory(category).subscribe(response => {
        if (language == 'es'){
          this.showSuccess(`La categoria ${response.category.name.es} se activo con exito`);
        } else if (language == 'en'){
          this.showSuccess(`La categoria ${response.category.name.en} se activo con exito`);
        }
        this.getData();
        this.loading.hide();
      }, error => {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
        console.log(error);
        this.loading.hide();
      });
    }
  }

  delete(category: string){
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    this.service.deleteCategory(category).subscribe(response => {
      if (language == 'es'){
        this.showSuccess(`La categoria ${response.category.name.es} se elimino con exito`);
      } else if (language == 'en'){
        this.showSuccess(`La categoria ${response.category.name.en} se elimino con exito`);
      }
      this.getData();
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light' });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light' });
  }

}

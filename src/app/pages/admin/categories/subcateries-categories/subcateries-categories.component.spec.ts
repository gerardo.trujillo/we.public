import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubcateriesCategoriesComponent } from './subcateries-categories.component';

describe('SubcateriesCategoriesComponent', () => {
  let component: SubcateriesCategoriesComponent;
  let fixture: ComponentFixture<SubcateriesCategoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubcateriesCategoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubcateriesCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

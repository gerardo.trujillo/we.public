import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { E403RoutingModule } from './e403-routing.module';
import { E403Component } from './e403.component';
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations: [
    E403Component
  ],
    imports: [
        CommonModule,
        E403RoutingModule,
        TranslateModule
    ]
})
export class E403Module { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { E403Component } from './e403.component';

const routes: Routes = [{ path: '', component: E403Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class E403RoutingModule { }

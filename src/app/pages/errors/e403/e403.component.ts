import { Component, OnInit } from '@angular/core';
import { StorageService } from "../../../services/storage/storage.service";

@Component({
  selector: 'app-e403',
  templateUrl: './e403.component.html',
  styleUrls: ['./e403.component.sass']
})
export class E403Component implements OnInit {

  constructor(public storage: StorageService) { }

  ngOnInit(): void {
  }

}

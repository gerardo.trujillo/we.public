import { Component, OnInit } from '@angular/core';
import { faUnlock } from "@fortawesome/free-solid-svg-icons";
import { User } from "../../../../interfaces/user";
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute, Router } from "@angular/router";
import { StorageService } from "../../../../services/storage/storage.service";
import { PasswordService } from "../../../../services/auth/password.service";
import { ToastService } from "../../../../services/components/toast.service";
import { Title } from "@angular/platform-browser";
import { NgForm } from "@angular/forms";
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-find-user',
  templateUrl: './find-user.component.html',
  styleUrls: ['./find-user.component.sass']
})
export class FindUserComponent implements OnInit {

  faUnlock = faUnlock;
  user:User=<User>{};
  disabled:boolean=false;
  url:string=environment.frontUrl;
  formInvalid:boolean=true;

  constructor(private loading: NgxSpinnerService,
              private title: Title,
              private activatedRoute: ActivatedRoute,
              private storage: StorageService,
              private service: PasswordService,
              private toastService: ToastService,
              private router: Router) {
    this.title.setTitle('Recuperar contraseña');
  }

  ngOnInit(): void {
  }

  submit(form: NgForm){
    this.loading.show();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('email', form.value.email);
    params.append('url', this.url);
    this.service.findUser(params).subscribe( response => {
      this.showSuccess('Se envio un mensaje a su correo, donde podra recuperar su contraseña');
      this.router.navigateByUrl('/sign/login');
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error al ingresar al usuario, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 5000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 5000 });
  }

}

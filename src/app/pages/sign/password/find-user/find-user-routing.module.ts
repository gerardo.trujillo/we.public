import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FindUserComponent } from './find-user.component';

const routes: Routes = [{ path: '', component: FindUserComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FindUserRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FindUserRoutingModule } from './find-user-routing.module';
import { FindUserComponent } from './find-user.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { FormsModule } from "@angular/forms";


@NgModule({
  declarations: [
    FindUserComponent
  ],
  imports: [
    CommonModule,
    FindUserRoutingModule,
    FontAwesomeModule,
    FormsModule
  ]
})
export class FindUserModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PasswordComponent } from './password.component';
import { ForcePasswordGuard } from "../../../guards/force-password.guard";

const routes: Routes = [
  {
    path: '',
    component: PasswordComponent,
    children: [
      {
        path: 'force',
        canActivate: [ ForcePasswordGuard ],
        loadChildren: () => import('./force/force.module').then(m => m.ForceModule)
      },
      {
        path: 'reset/:token',
        loadChildren: () => import('./reset/reset.module').then(m => m.ResetModule)
      },
      {
        path: 'find/user',
        loadChildren: () => import('./find-user/find-user.module').then(m => m.FindUserModule)
      },
      {
        path: '',
        redirectTo:'find/user'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PasswordRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { faEye, faEyeSlash, faSignInAlt } from "@fortawesome/free-solid-svg-icons";
import { NgForm } from "@angular/forms";
import { User } from "../../../../interfaces/user";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { StorageService } from "../../../../services/storage/storage.service";
import { ToastService } from "../../../../services/components/toast.service";
import { PasswordService } from "../../../../services/auth/password.service";
import { Title } from "@angular/platform-browser";
import {AuthService} from "../../../../services/auth/auth.service";

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.sass']
})
export class ResetComponent implements OnInit {

  faSignInAlt = faSignInAlt;
  faPassword = faEye;
  typePassword:string='password';
  user:User=<User>{};
  disabled:boolean=false;
  url:string='';
  token:string='';
  classPassword:string='';
  classPass:string='';
  formInvalid:boolean=true;

  constructor(private loading: NgxSpinnerService,
              private title: Title,
              private authService: AuthService,
              private activatedRoute: ActivatedRoute,
              private storage: StorageService,
              private service: PasswordService,
              private toastService: ToastService,
              private router: Router) {
    this.title.setTitle('Cambiar  Contraseña');
    this.activatedRoute.params.subscribe(params => {
      this.getData(params['token']);
      this.token =params['token'];
    });
  }

  ngOnInit(): void {
  }

  getData(token: string){
    this.loading.show();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('token', token);
    this.service.tokenFind(params).subscribe( response => {
      this.user = response.user;
      this.storage.setUser(response.user);
      this.storage.setToken(response.webToken);
      this.loading.hide();
    }, error => {
      console.log(error);
      if(error.status == 400){
        this.showDanger(error.error.msg);
        this.router.navigateByUrl('/sign/password/find/user');
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  submit(form: NgForm){
    this.loading.show();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('id', this.user.uid);
    params.append('password', form.value.password);
    params.append('token', this.token);
    this.service.changePasswordToken(params).subscribe( response => {
      this.authService.signIn();
      if(response.user.role == 'SUPER_ADMIN_ROLE'){
        this.url = '/admin';
      } else if(response.user.role == 'ADMIN_ROLE') {
        this.url = '/';
      }
      this.disabled = true;
      this.formInvalid = true;
      this.router.navigateByUrl(this.url);
      this.loading.hide();
    }, error => {
      console.log(error);
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  togglePassword(){
    if (this.typePassword == 'password'){
      this.typePassword = 'text';
      this.faPassword = faEyeSlash;
    } else {
      this.typePassword = 'password';
      this.faPassword = faEye;
    }
  }

  checkPassword(password: string, pass: string){
    if (password == pass){
      this.classPass = 'is-valid';
      this.formInvalid = false;
    } else {
      this.classPass = 'is-invalid';
      this.formInvalid = true;
    }
  }

  checkPasswordLength(password: string){
    if (password.length >= 8){
      this.classPassword = 'is-valid';
    } else {
      this.classPassword = 'is-invalid';
    }
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light' });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light' });
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForceRoutingModule } from './force-routing.module';
import { ForceComponent } from './force.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { FormsModule } from "@angular/forms";


@NgModule({
  declarations: [
    ForceComponent
  ],
  imports: [
    CommonModule,
    ForceRoutingModule,
    FontAwesomeModule,
    FormsModule
  ]
})
export class ForceModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForceComponent } from './force.component';

const routes: Routes = [{ path: '', component: ForceComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForceRoutingModule { }

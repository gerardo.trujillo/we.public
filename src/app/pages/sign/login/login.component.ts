import { Component, OnInit } from '@angular/core';
import { User } from "../../../interfaces/user";
import { faEye, faEyeSlash } from "@fortawesome/free-regular-svg-icons";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from "../../../services/auth/auth.service";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { StorageService } from "../../../services/storage/storage.service";
import { ToastService } from "../../../services/components/toast.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  user:User=<User>{};
  type = 'password';
  faPassword = faEye;

  constructor(private loading: NgxSpinnerService,
              private authService: AuthService,
              private toastService: ToastService,
              private router: Router,
              private storage: StorageService) {}

  ngOnInit(): void {
    localStorage.clear();
  }

  submit(form: NgForm){
    this.loading.show();
    let params = {
      email: form.value.email,
      password: form.value.password
    }
    this.authService.login(params).subscribe( response => {
      if (response.user.active && !response.user.delete ) {
        this.storage.setToken(response.token);
        this.storage.setUser(response.user);
        this.authService.signIn();
        this.router.navigateByUrl('/admin');
      } else {
        this.showDanger('El Usuario no tiene acceso a la aplicación');
      }
      this.loading.hide();
    }, error => {
      console.log(error);
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  getUser(token: string){

  }

  togglePassword(){
    if (this.type == 'password'){
      this.type = 'text';
      this.faPassword = faEyeSlash;
    } else {
      this.type = 'password';
      this.faPassword = faEye;
    }
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light' });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light' });
  }

}
